#!/usr/bin/env python

import fire
import pricat
import pandas as pd
import os.path, sys, glob
from pathlib import Path

def import_file(filename, country=None, target_path="data", sep=";"):
    f = pricat.File()
    check = f.validate_file(filename)
    print(check)
    p=f.load_file(filename)
    lst = p["list"]
    relative_dir = os.path.join("DATE=%s" % p["header"]["DATE"], "COUNTRY=%s" % p["header"]["COUNTRY"])
    target_dir = os.path.join(target_path, "bronze/" + relative_dir)
    Path(target_dir).mkdir(parents=True, exist_ok=True)
    target_file = os.path.join(target_dir, os.path.basename(filename))
    lst.list.to_csv(target_file, sep=";", index=False)

    lst.parser()
    
    target_dir = os.path.join(target_path, "silver/" + relative_dir)
    Path(target_dir).mkdir(parents=True, exist_ok=True)
    target_file = os.path.join(target_dir, os.path.basename(filename))
    lst.list.to_csv(target_file, sep=sep, index=False)

    ## append all silver files 
    target_dir = os.path.join(target_path, "gold")
    Path(target_dir).mkdir(parents=True, exist_ok=True)
    target_file = os.path.join(target_dir, "pricat_catalog.csv")
    export(target_file, sep=sep)
    
def export(target_file, sep=";"):
    files = glob.glob("data/silver/*/*/*.*")
    df = pd.concat([pd.read_csv(f, sep=sep) for f in files], ignore_index = True, sort=True)
    df.to_csv(target_file, sep=sep, index=False)

def main():
    fire.Fire({
        'import': import_file,
        })
 ##   filename = sys.argv[1]
 ##   import_file(filename)
    
if __name__ == "__main__":
    main()

